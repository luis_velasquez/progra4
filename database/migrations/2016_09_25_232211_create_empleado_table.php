<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->string('carnet')->primary();
            $table->string('nombre', 50);
            $table->string('apellido', 50);
            $table->string('direccion_residencia', 70);
            $table->string('unidades_administrativa_id', 30);
            $table->foreign('unidades_administrativa_id')->references('nombre_unidad')->on('unidades_administrativas')->onUpdate('cascade');
            $table->string('cargo_id', 50);
            $table->foreign('cargo_id')->references('cargo_nombre')->on('cargos')->onUpdate('cascade');
            $table->integer('municipio_id');
            $table->foreign('municipio_id')->references('idmunicipio')->on('municipios')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleados');
    }
}
