<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->string('placa')->primary();
            $table->boolean('activo')->default(0);
            $table->integer('anio');
            $table->string('tarjeta_circulacion');
            $table->date('tarjeta_expiracion');
            $table->integer('kilometraje');
            $table->date('proximo_mante')->nullable();
            $table->string('foto')->nullable();
            $table->string('modelo_id', 50);
            $table->foreign('modelo_id')->references('modelo_nombre')->on('modelos')->onUpdate('cascade');
            $table->string('color', 50);
            $table->foreign('color')->references('color_nombre')->on('colors')->onUpdate('cascade');
            $table->string('estado', 50);
            $table->foreign('estado')->references('estado_nombre')->on('estado_vehiculos')->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehiculos');
    }
}
