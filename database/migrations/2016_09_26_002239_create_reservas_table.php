<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('codigo');
            $table->string('destino', 100);
            $table->string('direccion_destino');
            $table->timestamp('fecha_hora_salida')->nullable('NULL');
            $table->timestamp('fecha_hora_retorno')->nullable('NULL');
            $table->string('concepto', 100);
            $table->integer('cantidad_pasajeros');
            $table->string('observaciones_solicitante', 100)->nullable();
            $table->string('observaciones_administrador', 100)->nullable();
            $table->double('distancia_kilometros', 10,2)->nullable();
            $table->double('kilometraje_inicial', 10,2)->nullable();
            $table->double('kilometraje_final', 10,2)->nullable();
            $table->string('motorista_id', 50);
            $table->foreign('motorista_id')->references('licencia')->on('motoristas')->onUpdate('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->string('tipo_reserva_id', 50);
            $table->foreign('tipo_reserva_id')->references('tipo_nombre')->on('tipo_reservas')->onUpdate('cascade');
            $table->string('estado_reserva_id', 50);
            $table->foreign('estado_reserva_id')->references('estado_nombre')->on('estado_reservas')->onUpdate('cascade');
            $table->integer('municipio_id');
            $table->foreign('municipio_id')->references('idmunicipio')->on('municipios')->onUpdate('cascade');
            $table->string('vehiculo_id', 50);
            $table->foreign('vehiculo_id')->references('placa')->on('vehiculos')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservas');
    }
}
