<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->username = 'Admin';
        $user->nombre = 'Francisco';
        $user->apellido = 'Gavidia';
        $user->email = 'ufg@ufg.com';
        $user->password = bcrypt('progra4');
        $user->save();
        $user->assignRole('Administrador');
        //here we create the profile picture
        $img = \DefaultProfileImage::create($user->nombre .' '. $user->apellido, 256, "#f68b1f", "#00529c");
        \Storage::put($user->id.'_profile.png', $img->encode());
    }
}
