@extends('layouts.reserva')
@section('customCss')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Reservas</h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Listado de Reservas</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <table id="reservas-table" class="table table-striped projects">
              <thead>
                <tr>
                  <th style="width: 20%">Mision</th>
                  <th>Usuario</th>
                  <th>Motorista</th>
                  <th>Estado</th>
                  <th style="width: 20%">#Edit</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($reservas as $reserva)
                <tr>
                  <td>
                    <a>{{ucfirst($reserva->destino)}}</a>
                    <br />
                    <small>{{ Carbon\Carbon::parse($reserva->created_at)->format('d-m-Y ') }}</small>
                  </td>
                  <td>
                    <ul class="list-inline">
                      <li>
                        <img src="/profile/{{$reserva->user->id}}" class="avatar" title="{{$reserva->user->nombre}} {{$reserva->user->apellido}}" alt="Avatar">
                      </li>
                    </ul>
                  </td>
                  <td>
                    <ul class="list-inline">
                      <li>
                        <img src="/motorista-foto/{{$reserva->motorista_id}}" title="{{$reserva->motorista->user->nombre}} {{$reserva->motorista->user->apellido}}" class="avatar" alt="Avatar">
                      </li>
                    </ul>
                  </td>
                  <td class="project_progress">
                    <div class="progress progress_sm">
                      <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{{$reserva->porcentaje}}"></div>
                    </div>
                    <small>{{$reserva->porcentaje}}% Completado</small>
                  </td>
                  {{-- <td>
                    <button type="button" class="btn btn-success btn-xs">Success</button>
                  </td> --}}
                  <td>
                    <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Ver </a>
                    @can('Acceso a todo', 'Crear Reserva')
                    <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                    <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Borrar </a>
                    @endrole
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/js/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/js/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="/js/reservas/index.js"></script>
@endsection
