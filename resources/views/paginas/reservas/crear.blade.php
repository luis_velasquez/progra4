@extends('layouts.reserva')
@section('customCss')
<link href="/js/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
<link href="/js/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
<link href="/js/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
<link href="/js/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<style type="text/css">
    .no-dispo {
        -webkit-filter: grayscale(100%); /* Chrome, Safari, Opera */
        filter: grayscale(100%);
        display: none !important;
    }
    .imagenes {
        box-shadow: 8px 8px 10px #aaa;
        margin: 3px;
    }
    .selectedImg {
        border-color: #428bca;
        border-style: solid;
        border:  3px;
    }
</style>
@endsection
@section('content')
<input type="hidden" id="csrf-token" value="{{ csrf_token() }}">
<div class="page-title">
  <div class="title_left">
    <h3>Reservaciones</h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Crear Reservacion</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
            <!-- Smart Wizard -->
            <p>Aqui puedes hacer tu reservacion</p>
            <div id="wizard" class="form_wizard wizard_horizontal">
              <ul class="wizard_steps">
                <li>
                  <a href="#step-1">
                    <span class="step_no">1</span>
                    <span class="step_descr">
                          Paso 1<br />
                          <small>Destino y Horas</small>
                      </span>
                  </a>
                </li>
                <li>
                  <a href="#step-2">
                    <span class="step_no">2</span>
                    <span class="step_descr">
                          Paso 2<br />
                          <small>Motorista y Automovil</small>
                      </span>
                  </a>
                </li>
                {{-- <li>
                  <a href="#step-3">
                    <span class="step_no">3</span>
                    <span class="step_descr">
                                      Step 3<br />
                                      <small>Step 3 description</small>
                                  </span>
                  </a>
                </li>
                <li>
                  <a href="#step-4">
                    <span class="step_no">4</span>
                    <span class="step_descr">
                                      Step 4<br />
                                      <small>Step 4 description</small>
                                  </span>
                  </a>
                </li> --}}
              </ul>
              <div id="step-1">
                <form class="form-horizontal form-label-left">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Destino <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input v-model="destino" type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Direccion de destino <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <textarea name="" class="resizable_textarea form-control" v-model="direccion_destino"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha / Hora de Salida</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" v-model="fecha_hora_salida" name="fecha_hora_salida" class="form-control has-feedback-left active" id="single_cal1" placeholder="Hora de Salida" aria-describedby="inputSuccess2Status" value="">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Fecha Hora de Retorno</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" v-model="fecha_hora_retorno" name="fecha_hora_retorno" class="form-control has-feedback-left active" id="single_cal1" placeholder="Hora de Salida" aria-describedby="inputSuccess2Status" value="">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Concepto <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <textarea name="" class="resizable_textarea form-control" v-model="concepto"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cantidad de Pasajeros <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input v-model="cantidad_pasajeros" type="number" id="first-name" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                  </div>
                </form>
              </div>
              <div id="step-2" style="display: none">
                <h2 class="StepTitle">Escoja Motorista</h2>
                <div class="col-md-12">
                  <ul class="list-inline">
                      @foreach ($motoristas as $motorista)
                      <li class="motoristasLi">
                          <img src="/motorista-foto/{{$motorista->licencia}}" class="" 
                          title="{{$motorista->user->nombre}} {{$motorista->user->apellido}}"
                          v-on:click="motoId('imagenes', {{$motorista->licencia}}, {{$loop->index}})"></li>
                      @endforeach
                  </ul>
                </div>
                <h2 class="StepTitle">Escoja Automovil</h2>
                <div class="col-md-12">
                  <ul class="list-inline">
                      @foreach ($vehiculos as $vehiculo)
                      <li class="vehiculosLi {{($vehiculo->activo == 1 ? 'no-dispo': '')}}">
                          <img class="" src="/vehiculo-foto/{{$vehiculo->placa}}" 
                          title="{{$vehiculo->modelo->modelo_nombre}}"
                          v-on:click="vehId('imagenes', {{$vehiculo->placa}}, {{$loop->index}})"></li>
                      @endforeach
                  </ul>
                </div>
              </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('customJs')
<script src="/js/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="/js/vendors/moment/min/moment.min.js"></script>
<script src="/js/vendors/select2/dist/js/select2.full.min.js"></script>
<script src="/js/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" charset="utf-8" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.27/vue.min.js" type="text/javascript" charset="utf-8" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.min.js"></script>
<!-- PNotify -->
<script src="/js/vendors/pnotify/dist/pnotify.js"></script>
<script src="/js/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/js/vendors/pnotify/dist/pnotify.nonblock.js"></script>
<script>
        var Vue = new Vue({
            el: 'body',
            http: {
                headers: {
                  'X-CSRF-TOKEN': $('#csrf-token').val()
                }
              },
            data: {
                destino: '',
                direccion_destino: '',
                fecha_hora_salida: '',
                fecha_hora_retorno: '',
                concepto: '',
                cantidad_pasajeros: '',
                motoSelected: '',
                vehiSelected: '',
            },
            ready: function() {

            },
            methods: {
                motoId: function(className, id, pos) {
                    $('.motoristasLi').removeClass('imagenes');
                    var el = document.getElementsByClassName('motoristasLi')[pos];
                    el.classList.toggle(className);
                    this.motoSelected = id;
                },
                vehId: function(className, id, pos) {
                    $('.vehiculosLi').removeClass('imagenes');
                    var el = document.getElementsByClassName('vehiculosLi')[pos];
                    el.classList.toggle(className);
                    this.vehiSelected = id;
                },
                noty: function(title, text, type) {
                    PNotify.prototype.options.styling = "fontawesome";
                    new PNotify({
                        title: title,
                        text: text,
                        type: type,
                        delay: 3000
                    });
                },
                step1: function() {
                    if(this.destino == '') {
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.direccion_destino == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.direccion_destino == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.fecha_hora_salida == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.fecha_hora_retorno == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.concepto == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else if(this.cantidad_pasajeros == ''){
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                        return false;
                    }
                    else {
                        return true;
                    }
                },
                sendingData: function() {
                    if(this.motoSelected == '' || this.vehiSelected == '') {
                        this.noty('Alerta', 'Faltan Campos!', 'error');
                    }
                    else {
                        var data = {
                            destino: this.destino,
                            direccion_destino: this.direccion_destino,
                            fecha_hora_salida: this.fecha_hora_salida,
                            fecha_hora_retorno: this.fecha_hora_retorno,
                            concepto: this.concepto,
                            cantidad_pasajeros: this.cantidad_pasajeros,
                            vehiculo_id: String(this.vehiSelected),
                            motorista_id: String(this.motoSelected),
                            municipio_id: 6,
                            user_id: {{Auth::id()}}
                        }
                        console.log(data);
                        this.$http.post('/reservas', {data})
                            .then(function(res){
                               console.log(res);
                               this.noty('Exito', 'Reservacion creada!', 'success');
                               setTimeout(function() {
                                    window.location.href = "/reservas";
                               }, 300);
                            });
                        
                    }
                }
            }
        })
    </script>
    <script>
      $(document).ready(function() {
        $('#wizard').smartWizard({
            labelNext:'Siguiente', // label for Next button
            labelPrevious:'Anterior', // label for Previous button
            labelFinish:'Terminar',  // label for Finish button 
            reverseButtonsOrder: false,
            hideButtonsOnDisabled: true,
            onLeaveStep: function(obj, context) {
                if(context.fromStep === 1){ 
                    return Vue.step1();
                }
                else {
                    return true;
                }
            },
            onFinish: function() {
                if(Vue.step1()) {
                    Vue.sendingData();
                }
            }
        });
        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
        // datepicker
        $('input[name="fecha_hora_salida"]').daterangepicker({
             singleDatePicker: true,
             showDropdowns: true,
             timePicker: true,
             timePickerIncrement: 30,
             locale: {
                 format: 'YYYY-MM-DD HH:mm:ss'
             }
         }, 
         function(start, end, label) {
             // var years = moment().diff(start, 'years');
             // alert("You are " + years + " years old.");
             $('input[name="tarjeta_expiracion"]').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
         });
        $('input[name="fecha_hora_retorno"]').daterangepicker({
             singleDatePicker: true,
             showDropdowns: true,
             timePicker: true,
             timePickerIncrement: 30,
             locale: {
                 format: 'YYYY-MM-DD HH:mm:ss'
             }
         }, 
         function(start, end, label) {
             // var years = moment().diff(start, 'years');
             // alert("You are " + years + " years old.");
             $('input[name="tarjeta_expiracion"]').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
         });
      });
    </script>
    
@endsection
