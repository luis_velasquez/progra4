@extends('layouts.reserva')
@section('customCss')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<link href="/js/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
@endsection
@section('content')
<input type="hidden" id="csrf-token" value="{{ csrf_token() }}">
    <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>

                      <div class="clearfix"></div>
                      @foreach ($vehiculos as $vehiculo)
                      <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>{{$vehiculo->modelo->modelo_nombre}}</i></h4>
                            <div class="left col-xs-7">
                              <h2>{{$vehiculo->modelo->marca->marca_nombre}}</h2>
                              <p>{{-- <strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover  --}}</p>
                              <ul class="list-unstyled">
                                {{-- <li><i class="fa fa-building"></i> Address: </li> --}}
                                <li><i class="fa fa-phone"></i> Placa: {{$vehiculo->placa}}</li>
                                <li><i class="fa fa-caret-right"></i> Año: {{$vehiculo->anio}}</li>
                                <li><i class="fa fa-caret-right"></i> Color: {{$vehiculo->color}}</li>
                                <li><i class="fa fa-car"></i> Estado <input type="checkbox" class="js-switch"  {{ ($vehiculo->activo == 1 ? "checked":"") }}/ id="{{$vehiculo->placa}}"> </li>
                              </ul>
                            </div>
                            <div class="right col-xs-5 text-center">
                              <img src="/vehiculo-foto/{{$vehiculo['placa']}}" alt="" class="img-circle img-responsive">
                            </div>
                          </div>
                          <div class="col-xs-12 bottom text-center">
                            <div class="col-xs-12 col-sm-6 emphasis"></div>
                            <div class="col-xs-12 col-sm-12 emphasis">
                              {{-- <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                </i> <i class="fa fa-comments-o"></i> </button> --}}
                              <a href="/automoviles/{{$vehiculo->placa}}/edit" title="">
                                  <button type="button" class="btn btn-primary btn-xs">
                                    <i class="fa fa-user"> </i> Editar
                                  </button>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('customJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/vendors/switchery/dist/switchery.min.js"></script>
{{-- <script src="/js/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/js/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script> --}}
<script src="/js/vehiculos/index.js"></script>
@endsection
