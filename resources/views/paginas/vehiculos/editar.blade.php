@extends('layouts.reserva')
@section('customCss')
<link href="/js/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Editar Marca</h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <form id="cargo-form" method="POST" action="/automoviles/{{ $vehiculo->placa }}" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Placa<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="placa" value="{{ $vehiculo->placa}}" id="placa" required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Año<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="anio" value="{{ $vehiculo->anio }}" id="anio" required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tarjeta de Circulacion<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="tarjeta_circulacion" value="{{ $vehiculo->tarjeta_circulacion }}" id="tarjeta_circulacion" required="required" class="form-control col-md-7 col-xs-12">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Expiracion de Tarjeta<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="tarjeta_expiracion" class="form-control has-feedback-left active" id="single_cal1" placeholder="Fecha de Expiracion" aria-describedby="inputSuccess2Status" value="{{$vehiculo->tarjeta_expiracion}}">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>
              </div>
                {{-- div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                      <input type="text" name="tarjeta_expiracion" class="form-control has-feedback-left active" id="single_cal1" placeholder="Fecha de Expiracion" aria-describedby="inputSuccess2Status">
                      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      <span id="inputSuccess2Status" class="sr-only">(success)</span>
                    </div>
                  </div>
                </div> --}}
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kilometraje<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="number" name="kilometraje" value="{{ $vehiculo->kilometraje }}" id="kilometraje" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Proximo Mantenimiento<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="proximo_mante" class="form-control has-feedback-left active" id="single_cal1" placeholder="Proximo Mantenimiento" aria-describedby="inputSuccess2Status" value="{{$vehiculo->proximo_mante}}">
                      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fotografia<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <img src="/vehiculo-foto/{{$vehiculo->placa}}" alt="">
                    <input type="file" name="fotografia" id="fotografia">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Modelo<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control modelo" name="modelo_id">
                        @foreach ($modelos as $modelo)
                          <option value="{{$modelo->modelo_nombre}}" {{ ($vehiculo->modelo_id == $modelo->modelo_nombre ? "selected":"") }}>{{$modelo->modelo_nombre}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Color<span class="required">*</span>
                  </label>
                  <div class="col-md- col-sm-6 col-xs-12">
                    <select class="form-control color" name="color">
                        @foreach ($colores as $color)
                          <option value="{{$color->color_nombre}}" {{ ($vehiculo->color == $color->color_nombre ? "selected":"") }}>{{$color->color_nombre}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Estado<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control color estado" name="estado">
                        @foreach ($estados as $estado)
                          <option value="{{$estado->estado_nombre}}" {{ ($vehiculo->estado == $estado->estado_nombre ? "selected":"") }}>{{$estado->estado_nombre}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                  <a href="/automoviles" title="">
                      <button type="button" class="btn btn-primary">Cancel</button>
                  </a>
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customJs')
<script src="/js/vendors/moment/min/moment.min.js"></script>
<script src="/js/vendors/select2/dist/js/select2.full.min.js"></script>
<script src="/js/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" charset="utf-8" ></script>
<script src="/js/vehiculos/scripts.js" type="text/javascript" charset="utf-8" async defer></script>
@endsection
