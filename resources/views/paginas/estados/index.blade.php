@extends('layouts.reserva')
@section('customCss')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Listado de Estados</h3>
  </div>
</div>
<input type="hidden" id="csrf-token" value="{{ csrf_token() }}">
<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <table id="estados-table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($estados as $estado)
                    <tr>
                        <td>{{$estado->estado_nombre}}</td>
                        <td>
                            <a href="/estados/{{$estado->estado_nombre}}/edit" title="">
                                <button class="btn btn-info btn-sm" type="button" name="button">Editar</button>
                            </a>
                            <button class="btn btn-danger btn-sm delete" estado-id="{{$estado->estado_nombre}}" type="button" name="button">Borrar</button>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('customJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/js/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/js/estados/index.js"></script>
@endsection
