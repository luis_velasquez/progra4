@extends('layouts.reserva')
@section('customCss')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection
@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Usuarios</h3>
  </div>
</div>

<div class="clearfix"></div>
<input type="hidden" id="csrf-token" value="{{ csrf_token() }}">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <a href="/usuarios/create" ><button type="button" class="btn btn-primary">Agregar Usuario</button></a>
        {{-- <ul class="nav navbar-right panel_toolbox">
          <li>
            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul> --}}
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <table id="usuarios-table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Usuario</th>
                  <th>Correo Electronico</th>
                  <th>Rol</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario->username}}</td>
                        <td>{{$usuario->email}}</td>
                        <td>{{$usuario->rol}}</td>
                        <td>
                            <a href="/usuarios/{{$usuario->id}}/edit" title="">
                                <button class="btn btn-info btn-sm" type="button" name="button">Editar</button></a>
                            <button class="btn btn-danger btn-sm delete" usuario-id="{{$usuario->id}}" type="button" name="button">Borrar</button>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customJs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/js/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="/js/usuarios/index.js"></script>
@endsection
