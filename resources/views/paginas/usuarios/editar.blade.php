@extends('layouts.reserva')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Editar Usuario</h3>
  </div>
</div>

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <form id="usuarios-form" method="POST" action="/usuarios/{{ $usuario->id }}" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {{ csrf_field() }}
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="id" value="{{ $usuario->id }}">
              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="nombre" class="col-md-4 control-label">Username</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="username" value="{{ $usuario->username }}" required autofocus>

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="nombre" class="col-md-4 control-label">Nombre</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="nombre" value="{{$usuario->nombre }}" required autofocus>

                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Apellido</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="apellido" value="{{ $usuario->apellido }}" required autofocus>

                        @if ($errors->has('apellido'))
                            <span class="help-block">
                                <strong>{{ $errors->first('apellido') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $usuario->email }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Roles</label>

                    <div class="col-md-6">
                        <select name="rol" id="rolesSelector">
                            @foreach ($roles as $rol)
                                <option value="{{ $rol->name }}" {{ ($rolUser == $rol->name ? "selected":"") }}>{{ $rol->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                {{-- @if(isset($motorista['licencia'])) --}}
                <div class="form-group{{ $errors->has('licencia') ? ' has-error' : '' }}" style="display: none" id="licDiv">
                    <label class="col-md-4 control-label">Licencia</label>
                    <div class="col-md-6">
                      <input type="text" name="licencia" class="form-control" value="{{(isset($motorista['licencia']) ? $motorista['licencia'] : '') }}" >
                      @if ($errors->has('licencia'))
                            <span class="help-block">
                                <strong>{{ $errors->first('licencia') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}" style="display: none" id="telDiv">
                    <label class="col-md-4 control-label">Telefono</label>
                    <div class="col-md-6">
                      <input type="text" name="telefono" class="form-control" value="{{(isset($motorista['telefono']) ? $motorista['telefono'] : '') }}"">
                      @if ($errors->has('telefono'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('tipo_licencia') ? ' has-error' : '' }}" style="display: none" id="tipoDiv">
                    <label for="tipo_licencia" class="col-md-4 control-label">Tipo de Licencia</label>
                    @if(isset($motorista["tipo_licencia"]))
                    <div class="col-md-6" >
                        <select name="tipo_licencia" >
                                <option value="ligera" {{ ($motorista["tipo_licencia"] == "ligera" ? "selected":"") }}>Ligera</option>
                                <option value="pesada" {{ ($motorista["tipo_licencia"] == "pesada" ? "selected":"") }}>Pesada</option>
                        </select>
                    </div>
                    @else
                    <div class="col-md-6" >
                        <select name="tipo_licencia" >
                                <option value="ligera">Ligera</option>
                                <option value="pesada">Pesada</option>
                        </select>
                    </div>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('fotografia') ? ' has-error' : '' }}" style="display: none" id="fotDiv">
                    <label class="col-md-4 control-label">Fotografia</label>
                    <div class="col-md-6">
                      @if (isset($motorista['licencia']))
                        <img src="/motorista-foto/{{$motorista['licencia']}}" alt="Fotografia" style="max-height: 150px">
                      @endif
                      <input type="file" name="fotografia" id="fotografia">
                      @if ($errors->has('fotografia'))
                            <span class="help-block">
                                <strong>{{ $errors->first('fotografia') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                {{-- @endif --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" >

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <a href="/usuarios" title="">
                            <button type="button" class="btn btn-primary">Cancel</button>
                        </a>
                        <button type="submit" class="btn btn-success">
                            Actualizar
                        </button>
                    </div>
                </div>

            </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('customJs')
<script src="/js/usuarios/scripts.js"></script>
@endsection
