<ul class="nav side-menu">
  @can('Acceso a todo')
  <li><a href="/inicio"><i class="fa fa-home"></i> Inicio </a>
    {{-- <ul class="nav child_menu">
      <li><a href="index.html">Dashboard</a></li>
      <li><a href="index2.html">Dashboard2</a></li>
      <li><a href="index3.html">Dashboard3</a></li>
    </ul> --}}
  </li>
  <li><a><i class="fa fa-calendar"></i> Reservas <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/reservas">Listado de Reservas</a></li>
      <li><a href="/reservas/create">Crear Reserva</a></li>
      <li><a>Tipo de Reservas <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/tiporeservas">Listado de Tipos de Reservas</a>
            </li>
            <li><a href="/tiporeservas/create">Crear Tipo de Reserva</a>
            </li>
        </ul>
      </li>
      <li><a>Estados de Reservas <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/estados">Listado de Estados de Reservas</a>
            </li>
            <li><a href="/estados/create">Crear Estado de Reserva</a>
            </li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a><i class="fa fa-male"></i> Motoristas <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/motoristas">Listado de Motoristas</a></li>
      <li><a href="/motoristas/create">Agregar Motorista</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-car"></i> Automoviles <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/automoviles">Listado de Automoviles</a></li>
      <li><a href="/automoviles/create">Agregar Automovil</a></li>
      <li><a>Marcas <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/marcas">Listado de Marcas</a>
            </li>
            <li><a href="/marcas/create">Crear Marca</a>
            </li>
        </ul>
      </li>
      <li><a>Modelos <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/modelos">Listado de Modelos</a>
            </li>
            <li><a href="/modelos/create">Crear Modelo</a>
            </li>
        </ul>
      </li>
      <li><a>Colores <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/colores">Listado de Colores</a>
            </li>
            <li><a href="/colores/create">Crear Color</a>
            </li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a><i class="fa fa-user"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/usuarios">Listado de Usuarios</a></li>
      <li><a href="/usuarios/create">Agregar Usuario</a></li>
    </ul>
  </li>
  <li><a><i class="fa fa-users"></i> Empleados <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/empleados">Listado de Empleados</a></li>
      <li><a href="/empleados/create">Agregar Empleado</a></li>
      <li><a>Cargos <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/cargos">Listado de Cargos</a>
            </li>
            <li><a href="/cargos/create">Crear Cargo</a>
            </li>
        </ul>
      </li>
      <li><a>Unidad Administrativa<span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/unidadadmnin">Unidades Administrativas</a>
            </li>
            <li><a href="/unidadadmnin/create">Crear Unidad Administrativa</a>
            </li>
        </ul>
      </li>
    </ul>
  </li>
  <li><a href="reportes"><i class="fa fa-file-pdf-o"></i> Reportes </a>
  </li>
  <li><a><i class="fa fa-lock"></i> Seguridad <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/acl">ACL</a></li>
      <li><a>Roles <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/roles">Listado de Roles</a>
            </li>
            <li><a href="/roles/create">Crear Rol</a>
            </li>
        </ul>
      </li>
      <li><a>Permisos <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
            <li class="sub_menu"><a href="/permisos">Listado de Permisos</a>
            </li>
            <li><a href="/permisos/create">Crear Permiso</a>
            </li>
        </ul>
      </li>
    </ul>
  </li>
  @endcan

  @can('Acceso a reserva')
  <li><a><i class="fa fa-calendar"></i> Reservas <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
      <li><a href="/reservas">Listado de Reservas</a></li>
      @can('Crear Reserva')
      <li><a href="/reservas/create">Crear Reserva</a></li>
      @endcan
    </ul>
  </li>
  @endcan
</ul>