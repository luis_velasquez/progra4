<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/inicio', 'HomeController@index');
// Rutas para Unidad administrativa
Route::resource('unidadadmnin', 'UnidadAdmController' );
// Rutas para cargos
Route::resource('cargos', 'CargosController');
// Rutas para roles
Route::resource('roles', 'RolesController');
// Rutas para permisos
Route::resource('permisos', 'PermisosController');
// Rutas para usuarios
Route::resource('usuarios', 'UserController');
// Rutas para marcas
Route::resource('marcas', 'MarcasController');
// Rutas para modelos
Route::resource('modelos', 'ModelosController');
// Rutas para vehiculos
Route::resource('automoviles', 'VehiculosController');
// Rutas para reservas
Route::resource('reservas', 'ReservasController');
Route::resource('tiporeservas', 'TiporeservasController');
Route::resource('estados', 'EstadosController');
Route::resource('colores', 'ColoresController');

// Rutas para motoristas
Route::get('motoristas', 'MotoristaController@index');
Route::get('motoristas/create', 'UserController@create');
Route::get('motoristas/{id}/edit', 'UserController@edit');

// Rutas ACL
Route::resource('acl', 'AclController');
Route::get('getAcl', 'AclController@getAcl');
// Ruta para las imagenes de perfil
Route::get('/profile/{image}', 'FrontController@profilePictures');
Route::get('/motorista-foto/{image}', 'FrontController@motoristaPictures');
Route::get('/vehiculo-foto/{image}', 'FrontController@vehiculoPictures');
Route::get('/empleado-foto/{image}', 'FrontController@empleadoPictures');

Route::post('/vehiculos-estado/{id}', 'VehiculosController@estado');
// Notificaciones
Route::post('/notificaciones', 'NotificacionesController@reservasCreadas');
//Capturando todas las rutas que no existen
Route::any( '{catchall}', function ( $page ) {
    // dd( $page . ' requested' );
    return view('errors.404');
} )->where('catchall', '(.*)');
