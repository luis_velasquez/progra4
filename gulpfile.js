const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
        // .scripts([
        //     'vendors/jquery/dist/jquery.js',
        //     'vendors/bootstrap/dist/js/bootstrap.min.js',
        //     'vendors/fastclick/lib/fastclick.js',
        //     'vendors/nprogress/nprogress.js',
        //     'vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
        //     'custom.min.js',
        // ])
        // .copy('vendors', 'public/css/')
       .webpack('app.js')
       .webpack('notis.js');
});
