<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidadadm extends Model
{
	  protected $table = 'unidades_administrativas';

    protected $primaryKey = 'nombre_unidad'; 

  public $incrementing = false;

    protected $fillable = [
        'nombre_unidad',
    ];
}
