<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $primaryKey = 'cargo_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'cargos_nombre',
    ];
}
