<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $primaryKey = 'modelo_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'modelo_nombre',
        'marca_id',
    ];

    public function marca()
    {
        return $this->belongsTo('App\Marca');
    }
}
