<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $primaryKey = 'marca_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'marca_nombre',
    ];
}
