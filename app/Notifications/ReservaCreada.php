<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

use App\Reserva;
use App\User;

class ReservaCreada extends Notification
{
    use Queueable;
    protected $reserva;
    protected $userId;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Reserva $reserva, $userId)
    {
        $this->reserva = $reserva;
        $this->userId = $userId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', 'https://laravel.com')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmo($notifiable)
    {
        $user = User::find($this->userId);
        $nombre = $user->nombre . ' ' . $user->apellido;
        return (new NexmoMessage)
                    ->content('Nueva reserva creada por parte de ' . $nombre .'.');
    }

    // public function toBroadcast($notifiable)
    // {
    //     return $this->reserva->toArray();
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $data['reserva'] = $this->reserva->toArray();
        $user = User::find($this->userId);
        $data['userName'] = $user->nombre . ' ' . $user->apellido;
        return $data;
    }
}
