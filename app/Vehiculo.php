<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $primaryKey = 'placa'; // or null

    protected $fillable = [
        'placa', 'anio', 'tarjeta_circulacion', 'tarjeta_expiracion', 'kilometraje',
        'proximo_mante', 'modelo_id', 'color', 'estado'
    ];

    public function modelo()
    {
        return $this->belongsTo('App\Modelo');
    }
}
