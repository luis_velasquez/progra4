<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $primaryKey = 'idmunicipio'; // or null

    protected $fillable = [
        'idmunicipio',
    ];
}
