<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $primaryKey = 'color_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'cargos_nombre',
    ];
}
