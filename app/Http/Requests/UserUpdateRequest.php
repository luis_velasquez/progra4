<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = Request::all();
        if($data['rol'] == 'Motorista') { 
            return [
                'username' => 'required|max:255|unique:users,username,'.$data['id'],
                'nombre' => 'required|max:255',
                'apellido' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,'.$data['id'],
                'licencia' => 'required|unique:motoristas,licencia,'.$data['licencia'] . ',licencia',
                'tipo_licencia' => 'required|max:50',
                'password' => 'min:6|confirmed',
            ];
        }
        else {
            return [
                'username' => 'required|max:255|unique:users,username,'.$data['id'],
                'nombre' => 'required|max:255',
                'apellido' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,'.$data['id'],
                'password' => 'min:6|confirmed',
            ];
        }

        
    }
}
