<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class VehiculosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $data = Request::all();
        // dd($data);
        return [
            'placa' => 'required|max:255|unique:vehiculos',
            'anio' => 'required|max:4|string',
            'tarjeta_circulacion' => 'required|max:255|unique:vehiculos',
            'tarjeta_expiracion' => 'required|date',
            'kilometraje' => 'numeric',
            'proximo_mante' => 'date',
            'fotografia' => 'file',
        ];
    }
}
