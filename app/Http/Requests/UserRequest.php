<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = Request::all();
        if($data['rol'] == 'Motorista') {
            return [
                'username' => 'required|max:255|unique:users',
                'nombre' => 'required|max:255',
                'apellido' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'licencia' => 'required|unique:motoristas',
                'tipo_licencia' => 'required|max:50',
                'fotografia' => 'required',
                'password' => 'required|min:6|confirmed',
            ];
        }
        else {
            return [
                'username' => 'required|max:255|unique:users',
                'nombre' => 'required|max:255',
                'apellido' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
            ];
        }
    }
}
