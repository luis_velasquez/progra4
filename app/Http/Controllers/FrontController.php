<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FrontController extends Controller
{
    // Images for the user profile
    public function profilePictures($filename)
    {
        $path = storage_path().'/app/'.$filename.'_profile.png';

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }
    public function vehiculoPictures($filename)
    {
        $path = storage_path().'/app/'.$filename.'_vehiculo.png';

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }

    public function motoristaPictures($filename)
    {
        $path = storage_path().'/app/'.$filename.'.jpg';

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }

    public function empleadoPictures($filename)
    {
        $path = storage_path().'/app/'.$filename.'_empleado.png';

        $file = \File::get($path);
        $type = \File::mimeType($path);

        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }
}
