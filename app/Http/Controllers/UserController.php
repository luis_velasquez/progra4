<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Motorista;
use Spatie\Permission\Models\Role;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = User::all();
        $users = array();

        foreach ($allUser as $key => $user) {
            $users[$key] = $user;
            $users[$key]['rol'] = $user->roles()->pluck('name')[0];
        }
        return view('paginas/usuarios/index', ['usuarios' => $users] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/usuarios/crear', ['roles'    => Role::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->all();
        $telefono = (isset($data['telefono']) ? $data['telefono'] : '');
        // dd(storage_path(). '/app/' . $data['licencia'].'.jpg');
        $user = User::create([
            'username' => $data['username'],
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'telefono' => $telefono,
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
        // dd($user->id);
        
        // asignamos el rol al usuario
        $user->assignRole($data['rol']);
        // revisamos si es motorista
        if($data['rol'] === 'Motorista') {
            Motorista::create([
               'licencia' => $data['licencia'],
               'tipo_licencia' => $data['tipo_licencia'],
               'user_id' => $user->id,
            ]);
            // Copiamos la imagen
            \Image::make($_FILES["fotografia"]['tmp_name'])->resize(100, 100)
                ->save(storage_path(). '/app/' . $data['licencia'].'.jpg');
        }
        //here we create the profile picture
        $img = \DefaultProfileImage::create($data['nombre'].' '.$data['apellido'], 256, "#f68b1f", "#00529c");
        \Storage::put($user->id.'_profile.png', $img->encode());

        return redirect()->back()->with('status', 'Usuario Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $rol = $user->roles()->pluck('name');
        if($rol[0] == 'Motorista')
        {
            $motorista = $user->motorista;
            return view('paginas/usuarios/editar', [
                            'usuario' => User::find($id),
                            'roles'   => Role::all(),
                            'rolUser' => $rol[0],
                            'motorista' => $motorista
                        ]);
        }
        else {
            return view('paginas/usuarios/editar', [
                            'usuario' => User::find($id),
                            'roles'   => Role::all(),
                            'rolUser' => $rol[0],
                            'motorista' => ''
                        ]);
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $data = $request->all();
        $user = User::find($id);
        $user->username = $data['username'];
        $user->nombre = $data['nombre'];
        $user->apellido = $data['apellido'];
        $user->email = $data['email'];
        if(isset($data['password']) and $data['password'] != '')
        {
            $user->password = bcrypt($data['password']);
        }
        $user->save();
        $user->syncRoles([$data['rol']]);
        if($data['rol'] === 'Motorista')
        {
            $motorista = $user->motorista;
            if($motorista === null) {
                Motorista::create([
                   'licencia' => $data['licencia'],
                   'telefono' => $data['telefono'],
                   'tipo_licencia' => $data['tipo_licencia'],
                   'user_id' => $user->id,
                ]);
            }
            else {
                $lastLic = $motorista->licencia;
                $motorista->licencia = $data['licencia'];
                $motorista->telefono = $data['telefono'];
                $motorista->tipo_licencia = $data['tipo_licencia'];
                $motorista->save();
            }
            if(isset($_FILES["fotografia"]['tmp_name']) and $_FILES["fotografia"]['tmp_name'] != '')
            {
                // Copiamos la imagen
            \Image::make($_FILES["fotografia"]['tmp_name'])->resize(100, 100)
                ->save(storage_path(). '/app/' . $data['licencia'].'.jpg');
            }
            else {
                if(!Storage::exists($data['licencia'].'.jpg'))
                {
                    if(isset($lastLic))
                    {
                        Storage::move(
                              $lastLic .'.jpg', 
                              $data['licencia'].'.jpg'
                         );
                    }
                    // else {
                    //     Storage::move(
                    //           $data['licencia'] .'.jpg', 
                    //           $data['licencia'].'.jpg'
                    //      );
                    // }       
                }
            }
        }
        return redirect()->back()->with('status', 'Usuario Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::find($id)->delete()) {
            return response()->json(['status' => 'true']);
        }
    }
}
