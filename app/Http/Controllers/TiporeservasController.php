<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App\TipoReserva;
use App\Http\Requests\StoreTiporeservas;

class TiporeservasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('paginas/tiporeservas/index', ['tiporeservas' => Tiporeserva::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/tiporeservas/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTiporeservas $request)
    {
        $data = $request->all();

        $tiporeserva = new TipoReserva();
        $tiporeserva->tipo_nombre = $data['tipo_nombre'];
        $tiporeserva->save();
        return redirect()->back()->with('status', 'Tipo Reserva Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('paginas/tiporeservas/editar')->with('tiporeserva', TipoReserva::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTiporeservas $request, $id)
    {
        $data = $request->all();
        $tiporeserva = TipoReserva::find($id);
        $tiporeserva->tipo_nombre = $data['tipo_nombre'];
        $tiporeserva->save();
        return redirect('/tiporeservas')->with('status', 'Tipo Reserva Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Tiporeserva::find($id)->delete()) {
            return response()->json(['status' => 'true']);
        }
    }
}
