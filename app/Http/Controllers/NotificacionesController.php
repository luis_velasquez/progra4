<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class NotificacionesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function reservasCreadas()
    {
        return \Auth::user()->unreadNotifications;
    }
}
