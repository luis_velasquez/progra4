<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Marca;
use App\Modelo;

class ModelosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('paginas/modelos/index', ['modelos' => Modelo::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/modelos/crear', ['marcas' => Marca::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $modelo = new Modelo();
        $modelo->modelo_nombre = $data['modelo_nombre'];
        $modelo->marca_id = $data['marca_id'];
        $modelo->save();
        return redirect()->back()->with('status', 'Modelo Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('paginas/modelos/editar', [
                        'modelo' => Modelo::find($id),
                        'marcas' => Marca::all()
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $data = $request->all();
        $modelo = Modelo::find($id);
        $modelo->modelo_nombre = $data['modelo_nombre'];
        $modelo->marca_id = $data['marca_id'];
        $modelo->save();
        return redirect('/modelos')->with('status', 'Modelo Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Modelo::find($id)->delete()) {
            return response()->json(['status' => 'true']);
        }
    }
}
