<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AclController extends Controller
{
    function __construct()
    {
      $this->role = new Role;
      $this->permission = new Permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('paginas.acl.index');
    }

    public function getAcl()
    {
        // We store all the roles
        $roles['roles'] = $this->role->all();
        // We loop the roles searching for permissions
        foreach ($roles['roles'] as $key => $role) {
          // We loop every role so we can use permission id for array index
          foreach ($role->permissions->all() as $k => $per) {
              $per['checked'] = true;
              // We store all the permissions using role id and per id as index
              $roles['rol_per'][$role->id][$per->id] = $per;
          }
        }

        return response()->json([
            'ro_pe' => $roles,
            'permissions' => $this->permission->all()
          ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // We check if state is true or false
        $data = $request->only('per', 'state');
        $role = Role::find($id);
        if($data['state'] == false) {
          $role->revokePermissionTo($data['per']);
          return response()->json(['sts' => 'true']);
        }
        else {
          $role->givePermissionTo($data['per']);
          return response()->json(['sts' => 'false']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}