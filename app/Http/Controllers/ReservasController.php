<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Reserva;
use App\Motorista;
use App\TipoReserva;
use App\EstadoReserva;
use App\Municipio;
use App\Vehiculo;
use App\Notifications\ReservaCreada;
use App\User;
use App\Events\UserCreoReserva;
use Carbon\Carbon;

class ReservasController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(\Auth::id());
        // se mostraran las reservas segun su rol
        if($user->hasRole('Administrador')) {
            $reservas = Reserva::with('user', 'motorista.user')->get();
        }
        elseif($user->hasRole('Empleado')) {
           $reservas = Reserva::where('user_id', $user->id)->with('user', 'motorista.user')->get();
        }
        else {
            $reservas = Reserva::where('motorista_id', $user->motorista->licencia)->with('user', 'motorista.user')->get();
        }
        // setting default time
        date_default_timezone_set('America/El_Salvador');
        // hora actual
        $nowDatetime = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        // sacando porcentaje
        foreach ($reservas as $key => $reserva) { 
            // comprobando si mision ha comenzado
            $time = Carbon::parse($nowDatetime)->diffInMinutes(Carbon::parse($reserva->fecha_hora_salida),false);
            if($time < 0) {
                //checando que hora de retorno no sea menor que actual
                if($nowDatetime->diffInMinutes(Carbon::parse($reserva->fecha_hora_retorno), false) < 0) {
                    // mision ha finalizado
                    $reservas[$key]['porcentaje'] = 100;
                }
                else {
                    // mision esta en progreso
                    $tiempoMision = Carbon::parse($reserva->fecha_hora_retorno)->diffInMinutes(Carbon::parse($reserva->fecha_hora_salida));
                    $tiempoTranscurrido = Carbon::parse($nowDatetime)->diffInMinutes(Carbon::parse($reserva->fecha_hora_salida));
                    $reservas[$key]['porcentaje'] =  round(( $tiempoTranscurrido /  $tiempoMision ) * 100);
                }
            }
            else {
                // mision no ha comenzado
                $reservas[$key]['porcentaje'] = 0;
            }
        }
        return view('paginas.reservas.index', [
                        'reservas' => $reservas
                    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas.reservas.crear', [
                        'motoristas' => Motorista::with('user')->get(),
                        'reservas'   => TipoReserva::all(),
                        'estados'    => EstadoReserva::all(),
                        'municipios' => Municipio::all(),
                        'vehiculos'  => Vehiculo::all(),
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reserva = Reserva::create($request->data);
        if(!$reserva) {
            throw new Exception('Error in saving data.');
        }
        else {
            $user = User::find(1);
            $user->notify(new ReservaCreada($reserva, \Auth::id()));
            event(new UserCreoReserva($user->unreadNotifications));
            //return redirect('/reservas')->with('status', '');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
