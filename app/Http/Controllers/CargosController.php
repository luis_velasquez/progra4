<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use View;
use App\Cargo;
use App\Http\Requests\StoreCargos;

class CargosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('paginas/cargos/index', ['cargos' => Cargo::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/cargos/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCargos $request)
    {
        $data = $request->all();

        $cargo = new Cargo();
        $cargo->cargo_nombre = $data['cargo_nombre'];
        $cargo->save();
        return redirect()->back()->with('status', 'Cargo Creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('paginas/cargos/editar')->with('cargo', Cargo::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCargos $request, $id)
    {
        $data = $request->all();
        $cargo = Cargo::find($id);
        $cargo->cargo_nombre = $data['cargo_nombre'];
        $cargo->save();
        return redirect('/cargos')->with('status', 'Cargo Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Cargo::find($id)->delete()) {
            return response()->json(['status' => 'true']);
        }
    }
}
