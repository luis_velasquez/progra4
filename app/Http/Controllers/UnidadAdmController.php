<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;

use App\Unidadadm;

use App\Http\Requests\StoreUnidad;


class UnidadAdmController extends Controller
{
	 public function __construct()
	{
		   $this->middleware('auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
            return view('paginas/unidadadministrativa/index',['unidadAdmin' => Unidadadm::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('paginas/unidadadministrativa/crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnidad $request)
    {
		
		$data = $request->all();
		$unidad = new Unidadadm();
		$unidad->nombre_unidad = $data['nombre_unidad'];
        $unidad->save();
		return redirect()->back()->with('status', 'Unidad administrativa Creada!');
        //dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       return view('paginas/unidadadministrativa/editar')->with('unidadadmin',  Unidadadm::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUnidad $request, $id)
    {
        $data = $request->all();
        $unidad= Unidadadm::find($id);
        $unidad->nombre_unidad = $data['nombre_unidad'];
        $unidad->save();
        return redirect('/unidadadmnin')->with('status', 'Unidad Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if (Unidadadm::find($id)->delete()) {
            return response()->json(['status' => 'true']);
        }
    }
}
