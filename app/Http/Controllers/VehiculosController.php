<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\VehiculosRequest;
use App\Http\Requests\VehiculosUpdateRequest;
use App\Vehiculo;
use App\Modelo;
use App\EstadoVehiculo;
use App\Color;
use Illuminate\Support\Facades\Storage;

class VehiculosController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('paginas/vehiculos/index')->with('vehiculos', Vehiculo::with('modelo.marca')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas/vehiculos/crear', [
                        'colores' => Color::all(),
                        'estados' => EstadoVehiculo::all(),
                        'modelos' => Modelo::all()
                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehiculosRequest $request)
    {
        $data = $request->all();
        Vehiculo::create($request->all());
        if(isset($_FILES["fotografia"]['tmp_name']) and $_FILES["fotografia"]['tmp_name'] != '')
        {
            \Image::make($_FILES["fotografia"]['tmp_name'])->resize(100, 100)
                ->save(storage_path(). '/app/' . $data['placa'].'_vehiculo.png');
        }
        return redirect()->back()->with('status', 'Vehiculo Agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('paginas/vehiculos/editar', [
                        'vehiculo' => Vehiculo::find($id),
                        'colores' => Color::all(),
                        'estados' => EstadoVehiculo::all(),
                        'modelos' => Modelo::all()
                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VehiculosUpdateRequest $request, $id)
    {
        $data = $request->all();
        $vehiculo = Vehiculo::find($id);
        $lastPlaca = $vehiculo->placa;
        $vehiculo->placa = $data['placa'];
        $vehiculo->anio = $data['anio'];
        $vehiculo->tarjeta_circulacion = $data['tarjeta_circulacion'];
        $vehiculo->tarjeta_expiracion = $data['tarjeta_expiracion'];
        $vehiculo->kilometraje = $data['kilometraje'];
        $vehiculo->proximo_mante = $data['proximo_mante'];
        $vehiculo->modelo_id = $data['modelo_id'];
        $vehiculo->color = $data['color'];
        $vehiculo->estado = $data['estado'];
        $vehiculo->save();
        if(isset($_FILES["fotografia"]['tmp_name']) and $_FILES["fotografia"]['tmp_name'] != '')
            {
                // Copiamos la imagen
            \Image::make($_FILES["fotografia"]['tmp_name'])->resize(100, 100)
                ->save(storage_path(). '/app/' . $data['placa'].'_vehiculo.png');
            }
            else {
                if(!Storage::exists($data['placa'].'_vehiculo.png'))
                {
                    if(isset($lastPlaca))
                    {
                        Storage::move(
                              $lastPlaca .'_vehiculo.png', 
                              $data['placa'].'_vehiculo.png'
                         );
                    }
                    // else {
                    //     Storage::move(
                    //           $data['licencia'] .'.jpg', 
                    //           $data['licencia'].'.jpg'
                    //      );
                    // }       
                }
            }
        
        return redirect('/automoviles')->with('status', 'Vehiculo Agregado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function estado($id, Request $request)
    {
        $vehiculo = Vehiculo::find($id);
        $vehiculo->activo = $request->status;
        $vehiculo->save();
        return response()->json([
            'status' => 'ok'
        ]);
    }
}
