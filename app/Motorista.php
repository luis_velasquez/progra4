<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motorista extends Model
{
    protected $primaryKey = 'licencia'; // or null

    public $incrementing = false;

    protected $fillable = [
        'licencia',
        'tipo_licencia',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
