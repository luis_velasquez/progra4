<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoVehiculo extends Model
{
    protected $primaryKey = 'estado_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'estado_nombre',
    ];
}
