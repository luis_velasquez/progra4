<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $primaryKey = 'codigo'; // or null

    public $incrementing = false;

    protected $fillable = [
        'codigo',
        'destino',
        'direccion_destino',
        'fecha_hora_salida',
        'fecha_hora_retorno',
        'concepto',
        'cantidad_pasajeros',
        'observaciones_solicitante',
        'observaciones_administrador',
        'distancia_kilometros',
        'kilometraje_inicial',
        'kilometraje_final',
        'motorista_id',
        'user_id',
        'tipo_reserva_id',
        'estado_reserva_id',
        'municipio_id',
        'vehiculo_id',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function motorista()
    {
        return $this->belongsTo('App\Motorista');
    }
}
