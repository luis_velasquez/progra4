<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoReserva extends Model
{
    protected $table = 'tipo_reservas';
    protected $primaryKey = 'tipo_nombre'; // or null

    public $incrementing = false;

    protected $fillable = [
        'tipo_nombre',
    ];
}
