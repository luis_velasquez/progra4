<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $primaryKey = 'carnet'; // or null

    public $incrementing = false;

    protected $fillable = [
        'carnet',
        'nombre',
        'apellido',
        'direccion_residencia',
        'unidades_administrativas',
        'cargo_id',
        'municipio_id',
    ];
}
