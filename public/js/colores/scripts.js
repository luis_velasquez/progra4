$(function() {
   var success = $('.alert-success').length; 
   var danger = $('.alert-danger').length; 
   if(success) {
    setTimeout(function() {
        $('.alert-success').slideUp();
    }, 3000);
   }
   else if(danger) {
    setTimeout(function() {
        $('.alert-danger').slideUp();
    }, 3000);
   }

});