$(function() {
   var success = $('.alert-success').length; 
   var danger = $('.alert-danger').length; 
   if(success) {
    $('.alert-success').delay(5000).slideUp();
   }
   else if(danger) {
    $('.alert-danger').delay(5000).slideUp();
   }
   setTimeout(function(){
    if($('#rolesSelector').val() === 'Motorista'){
         $('#licDiv').show();
         $('#telDiv').show();
         $('#tipoDiv').show();
         $('#fotDiv').show();
    }
   }, 800);
   
   $('#rolesSelector').change(function() {
        if($(this).val() === 'Motorista'){
            $('#licDiv').show();
            $('#telDiv').show();
            $('#tipoDiv').show();
            $('#fotDiv').show();
        }
        else {
            $('#licDiv').hide();
            $('#telDiv').hide();
            $('#tipoDiv').hide();
            $('#fotDiv').hide();
        }
   });
});