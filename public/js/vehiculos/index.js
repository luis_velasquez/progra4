$(function() {
    // Cambiando estado
    $('.js-switch').change(function () {
        var value = (this.checked) ? '1' : '0';
        var id = $(this).attr('id');
        $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('#csrf-token').val()
          },
          url: "/vehiculos-estado/" + id,
          type: "POST",
          data: {
            status: value
          }
        })
        .done(function(data) { 
            console.log(data);
        })
        .error(function(data) {
          console.log(data);
        });
     });
    // Detectando el boton de borrado
    $('.delete').click(function() {
       var id = $(this).attr('vehiculo-id');
       var dis = $(this);
       swal({
         title: 'Esta seguro?',
         text: "Ya no hay vuelta atras!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Si, borrelo.'
       }, function() {
             $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('#csrf-token').val()
               },
               url: "/cargos/" + id,
               type: "DELETE"
             })
             .done(function(data) { console.log(data);
                setTimeout(function() {
                    location.reload();
                }, 1100);
               swal("Deleted!", "Your file was successfully deleted!", "success");
               // We remove from the table the tr
               dis.parents('tr').remove();
             })
             .error(function(data) {
               swal("Oops", "We couldn't connect to the server!", "error");
             });
           });
    });
});