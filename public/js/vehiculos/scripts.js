$(function() {
   var success = $('.alert-success').length; 
   var danger = $('.alert-danger').length; 
   if(success) {
    setTimeout(function() {
        $('.alert-success').slideUp();
    }, 3000);
   }
   else if(danger) {
    setTimeout(function() {
        $('.alert-danger').slideUp();
    }, 3000);
   }
   $('input[name="tarjeta_expiracion"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, 
    function(start, end, label) {
        // var years = moment().diff(start, 'years');
        // alert("You are " + years + " years old.");
        $('input[name="tarjeta_expiracion"]').val(moment(start).format('YYYY-MM-DD'));
    });

   $('input[name="proximo_mante"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    }, 
    function(start, end, label) {
        $('input[name="proximo_mante"]').val(moment(start).format('YYYY-MM-DD'));
    });

   $(".color").select2();
   $(".modelo").select2();
   $(".estado").select2();

});