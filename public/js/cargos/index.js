$(function() {
    var table = $("#cargos-table").DataTable();
    // Detectando el boton de borrado
    $('.delete').click(function() {
       var id = $(this).attr('cargo-id');
       var dis = $(this);
       swal({
         title: 'Esta seguro?',
         text: "Ya no hay vuelta atras!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Si, borrelo.'
       }, function() {
             $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('#csrf-token').val()
               },
               url: "/cargos/" + id,
               type: "DELETE"
             })
             .done(function(data) { console.log(data);
                setTimeout(function() {
                    location.reload();
                }, 1100);
               swal("Deleted!", "Your file was successfully deleted!", "success");
               // We remove from the table the tr
               dis.parents('tr').remove();
             })
             .error(function(data) {
               swal("Oops", "We couldn't connect to the server!", "error");
             });
           });
    });
});