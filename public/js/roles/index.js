$(function() {
    var table = $("#roles-table").DataTable();
    // Detectando el boton de borrado
    $('.delete').click(function() {
       var id = $(this).attr('rol-id');
       var dis = $(this);
       swal({
         title: 'Esta seguro?',
         text: "Ya no hay vuelta atras!",
         type: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Si, borrelo.'
       }, function() {
             $.ajax({
               headers: {
                   'X-CSRF-TOKEN': $('#csrf-token').val()
               },
               url: "/roles/" + id,
               type: "DELETE"
             })
             .done(function(data) { console.log(data);
                setTimeout(function() {
                    location.reload();
                }, 1100);
               swal("Eliminado!", "El Rol fue exitosamente eliminado!", "success");
               // We remove from the table the tr
               dis.parents('tr').remove();
             })
             .error(function(data) {
               swal("Oops", "No pudimos conectarnos al servidor!", "error");
             });
           });
    });
});