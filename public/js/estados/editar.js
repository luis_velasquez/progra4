$(function() {
   var success = $('.alert-success').length; 
   var danger = $('.alert-danger').length; 
   if(success) {
    $('.alert-success').delay(5000).slideUp();
   }
   else if(danger) {
    $('.alert-danger').delay(5000).slideUp();
   }

});